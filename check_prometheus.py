#!/usr/bin/python

import argparse
import requests
import json

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

up = 0
down = 0
val = []
instance = []
desc = " "

def getValue(query,hostname,qtype,secure):
        global up
        global down
        global val
        global instance
        global desc

        endpoint = 'http' + secure + '://' + hostname + '/api/v1/query'
        payload = { "query" : query }
        response = requests.get(endpoint, params=payload, verify=False)
        json_prom = json.loads(response.text)
        #print json_prom

        if 'status' in json_prom:
                status = json_prom['status']

        if status == 'success':
                rtype = ""
                if 'resultType' in json_prom['data']:
                        rtype = json_prom['data']['resultType']
                        if rtype == "scalar" or rtype == "string":
                                value = json_prom['data']['result'][1]
                                if qtype == 'bool':
                                        if int(value) == 0:
                                                down += 1
                                                desc += "DOWN "
                                        else:
                                                up += 1
                                                desc += "UP "
                                elif qtype == 'count':
                                        val.append(value)
                else:
                        metrics = json_prom['data'][0]
                        for info in metrics:
                                if info == '__name__':
                                        continue
                                desc += info + ": " + metrics[info] + " "
                        # series endpoint for future feature
                        return(0)
                if rtype == "matrix" or rtype == "vector":
                        for entry in json_prom['data']['result']:
                                metrics = entry['metric']
                                for info in metrics:
                                        if info == '__name__':
                                                continue
                                        if qtype == 'count' and info == 'instance':
                                                instance.append(metrics[info])
                                        desc += info + ": " + metrics[info] + " "
                                value = entry['value'][1]
                                if qtype == 'bool':
                                        if int(value) == 0:
                                                down += 1
                                                desc += "DOWN "
                                        else:
                                                up += 1
                                                desc += "UP "
                                elif qtype == 'count':
                                        val.append(value)
                return(0)
        else:
                if qtype == 'bool':
                        desc = "UNKNOWN - Endpoint using query " + query + " returned " + status + " and " + str(down) + " instances are DOWN." + desc
                elif qtype == 'count':
                        desc = "UNKNOWN - Endpoint using query " + query + " returned " + status + ". " + desc
                return(3)

parser = argparse.ArgumentParser(description='Check Prometheus')
parser.add_argument('query', type=str, help='A valid Prometheus metric')
parser.add_argument('--host', action='store', dest='hostname', default='127.0.0.1', type=str, help='The hostname. Add :port as necessary .e.g. localhost:9090')
parser.add_argument('--grep', action='store', dest='grep', default='', type=str, help='Remove from instance name as part of performance data name')
parser.add_argument('--warn', action='store', dest='warn', default=1, type=float, help='The warning threshold')
parser.add_argument('--crit', action='store', dest='crit', default=2, type=float, help='The critical threshold')
parser.add_argument('--desc', action='store', dest='details', default=0, type=int, help='The length of details to be printed')
parser.add_argument('--qtype', default='bool', type=str, help='Use bool for metrics like up (can handle only 2 values returned by Prometheus: 0 - for DOWN, 1 - for UP). Use count otherwise.')
parser.add_argument('--secure', action='store_true', help='Use https instead of http protocol')
parser.add_argument('--negate', action='store_true', help='Negate the comparison operator.')
args = parser.parse_args()
secure=''
if args.secure:
	secure='s'
status = getValue(args.query,args.hostname,args.qtype,secure)
if args.qtype == 'bool':
        perf = "instances=" + str(up) + ";" + str(args.warn) + ";" + str(args.crit) + ";0;" + str(up + down)
        if down >= args.crit:
                print "CRITICAL - There are " + str(down) + " instances that are DOWN." + desc[:args.details] + " | " + perf
                status = 2
        elif down >= args.warn:
                print "WARNING - There are " + str(down) + " instances that are DOWN." + desc[:args.details] + " | " + perf
                status = 1
        elif down == 0:
                print "OK - All instances are UP." + desc[:args.details] + " | " + perf
                status = 0
        elif status == 0:
                print "OK - There are " + str(down) + " instances that are DOWN but we are still good to go." + desc[:args.details] + " | " + perf
        else:
                print desc[:args.details] + " | " + perf
elif args.qtype == 'count':
        crit = 0
        warn = 0
        perf = ""
        for i,j in zip(instance,val):
                perf += args.query + "_" + i.replace(args.grep,'') + "=" + j + " "
                if float(j) >= args.crit:
                        if(not args.negate):
                                crit += 1
                        else:
                                up += 1
                elif float(j) >= args.warn:
                        if(not args.negate):
                                warn += 1
                        else:
                                up += 1
                else:
                        if(not args.negate):
                                up += 1
                        else:
                                down += 1
                #print i + " " + j + " " + str(crit) + " " + str(args.crit) + " " + str(warn) + " " + str(args. warn) + " " + str(up) + " " + str(down)
        if crit > 0:
                print "CRITICAL - There are " + str(crit) + " instances that exceeds CRITICAL threshold." + desc[:args.details] + " | " + perf
                status = 2
        elif warn > 0:
                print "WARNING - There are " + str(warn) + " instances that exceeds WARNING threshold." + desc[:args.details] + " | " + perf
                status = 1
        elif up == len(instance):
                print "OK - All instances are OK." + desc[:args.details] + " | " + perf
                status = 0
        else:
                print desc[:args.details] + " | " + perf
exit(status)

