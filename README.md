# Nagios Plugins - Kubernetes

A collection of nagios plugins to check Kubernetes, Prometheus and other related Applications running In-Cluster  

Performance data is included.   

## Dependencies 
- Bash    
- Python  
  - json  
  - requests  
- kubectl  
  - export KUBECONFIG=/path/to/kube-config.yaml  
  - kubectl port-forward svc/kube-prometheus 9090:9090 -n monitoring  

## Scripts
- check_nodes  
  - Uses bash and kubectl to check all nodes  
- check_prometheus.py  
  - Uses python to query Prometheus.  

Testing against metrics listed on prometheus.metrics  

## Quick Start
- To check if how many nodes are up  
$ ./check_nodes -w 2 -c 1  
Nodes WARNING: 2 | nodes=2;2;1;0;3  
- To check if how many instances are up  
$ ./check_prometheus.py up  
WARNING - There are 1 instances that are DOWN. <some more data here> | instances=10;1;2;0;11   
- To check if how many instances of the kubelet service are up  
$ ./check_prometheus.py up{service=\"kubelet\"}  
OK - All instances are UP. <some more data here> | instances=2;1;2;0;2    $USER2$/check_prometheus.py --host $HOSTNAME$ node_procs_running --qtype count --warn $ARG1$ --crit $ARG2$ --grep $ARG3$  
$USER2$/check_prometheus.py --host $HOSTNAME$ node_netstat_Tcp_CurrEstab --qtype count --warn $ARG1$ --crit $ARG2$ --grep $ARG3$  

- To check if how many instances of the default namespace are up  
$ ./check_prometheus.py up{namespace=\"default\"}  
OK - All instances are UP. endpoint: https service: kubernetes namespace: default instance: <ip.add.re.ss>:443 job: apiserver UP | instances=1;1;2;0;1  
- Using warning and critical thresholds  
$ ./check_prometheus.py up --warn 2  
OK - There are 1 instances that are DOWN but we are still good to go.  <some more data here> | instances=10;2;2;0;11  
$ ./check_prometheus.py up --crit 1  
CRITICAL - There are 1 instances that are DOWN. <some more data here> | instances=10;1;1;0;11  
- Using hostname and non up metric  
$ ./check_prometheus.py --host prometheus.local.lan --qtype count node_load1  
OK - All instances are OK. | node_load1_10.0.14.173=0.14 node_load1_10.0.14.196=0.31 node_load1_10.0.14.244=0.29
## Usage
``check_prometheus.py [-h] [--host HOSTNAME] [--grep GREP] [--warn WARN]
                           [--crit CRIT] [--desc DETAILS] [--qtype QTYPE]
                           query``
## Sample Nagios Commands Config commands.cfg
$USER2$/check_prometheus.py --host $HOSTNAME$ $ARG1$  
e.g. $ARG1$ values = "up{k8s_app=\'kube-dns\'}", "up{kubernetes_io_role=\'node\'}", "up{job=\'kubernetes-apiservers\'}"  
$USER2$/check_prometheus.py --host $HOSTNAME$ $ARG1$ --qtype count --warn $ARG2$ --grep $ARG3$  
e.g. $ARG1$ values = node_load1, node_load5, node_load15  
$USER2$/check_prometheus.py --host $HOSTNAME$ instance:node_filesystem_usage:sum --qtype count --warn $ARG1$ --crit $ARG2$ --grep $ARG3$  
$USER2$/check_prometheus.py --host $HOSTNAME$ node_procs_running --qtype count --warn $ARG1$ --crit $ARG2$ --grep $ARG3$  
$USER2$/check_prometheus.py --host $HOSTNAME$ node_netstat_Tcp_CurrEstab --qtype count --warn $ARG1$ --crit $ARG2$ --grep $ARG3$  
The next two examples shows the use of --negate if we don't want the values be smaller than the values (normally we don't want the values to exceed the warn or crit values)  
$USER2$/check_prometheus.py --host $HOSTNAME$ node_memory_MemFree_bytes --qtype count --warn $ARG1$ --crit $ARG2$ --grep $ARG3$ --negate  
$USER2$/check_prometheus.py --host $HOSTNAME$ kubelet_volume_stats_available_bytes --qtype count --warn $ARG1$ --crit $ARG2$ --grep $ARG3$ --negate  
## Sample NRPE Config nrpe.cfg
command[check_grafana]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 up{service=\'kube-prometheus-grafana\'}  
command[check_alertmanager]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 up{service=\'kube-prometheus-alertmanager\'}  
command[check_prometheus]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 up{service=\'kube-prometheus\'}  
command[check_kubedns]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 up{service=\'kube-prometheus-exporter-kube-dns\'}  
command[check_kubernetes]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 up{service=\'kubernetes\'}  
command[check_kubelet]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 up{service=\'kubelet\'}  
command[check_kubestate]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 up{service=\'kube-prometheus-exporter-kube-state\'}  
command[check_node]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 up{service=\'kube-prometheus-exporter-node\'}  
command[check_load1]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 node_load1 --qtype count --warn 2.5 --crit 3  
command[check_load5]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 node_load5 --qtype count --warn 2 --crit 2.5  
command[check_load15]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 node_load15 --qtype count --warn 1.5 --crit 2  
command[check_memory]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 node_memory_MemFree --qtype count --warn 30000000 --crit 20000000 --negate  
command[check_filesystem]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 instance:node_filesystem_usage:sum --qtype count --warn 4000000000 --crit 5000000000  
command[check_procs]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 node_procs_running --qtype count --warn 4 --crit 5  
command[check_tcpconn]=/usr/lib/nagios/plugins/check_prometheus.py --host localhost:9090 node_netstat_Tcp_CurrEstab --qtype count --warn 20 --crit 25  
